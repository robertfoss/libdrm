/*
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * VA LINUX SYSTEMS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __ANDROID_ALLOC_HANDLE_H__
#define __ANDROID_ALLOC_HANDLE_H__

#include <cutils/native_handle.h>

/* support users of drm_gralloc/gbm_gralloc */
#define gralloc_gbm_handle_t alloc_handle_t
#define gralloc_drm_handle_t alloc_handle_t

struct alloc_handle_t {
	native_handle_t base;

	/* api variables */
	const int magic; /* differentiate between allocator impls */
	const int version; /* api version */

	int prime_fd; /* dma-buf file descriptor */
	int width; /* width of buffer in pixels */
	int height; /* height of buffer in pixels */
	int format; /* pixel format (fourcc) */
	int usage; /* android libhardware usage flags */

	int stride; /* the stride in bytes */
	uint64_t modifier; /* buffer modifiers */

	int data_owner; /* owner of data (for validation) */
	union {
		void *data; /* pointer to struct gralloc_gbm_bo_t */
		uint64_t reserved;
	} __attribute__((aligned(8)));
};

#define ALLOC_HANDLE_VERSION 4
#define ALLOC_HANDLE_MAGIC 0x60585350
#define ALLOC_HANDLE_NUM_FDS 1
#define ALLOC_HANDLE_NUM_INTS (	\
	((sizeof(struct alloc_handle_t) - sizeof(native_handle_t))/sizeof(int))	\
	 - ALLOC_HANDLE_NUM_FDS)

/**
 * Create a buffer handle.
 */
struct alloc_handle_t *alloc_handle_create(int width, int height, int format,
	                                       int usage)
{
	struct alloc_handle_t handle = {
		.magic = ALLOC_HANDLE_MAGIC,
		.version = ALLOC_HANDLE_VERSION };

	handle->base.version = sizeof(handle->base);
	handle->base.numInts = ALLOC_HANDLE_NUM_INTS;
	handle->base.numFds = ALLOC_HANDLE_NUM_FDS;

	handle->width = width;
	handle->height = height;
	handle->format = format;
	handle->usage = usage;
	handle->prime_fd = -1;

	handle->data_owner = getpid();
	handle->data = bo;

	return handle;
}

#endif
